﻿using Limiting.MiddleWares;
using Microsoft.AspNetCore.Builder;

namespace Limiting.Extensions
{
    public static class MiddlewareExtensions
    {
        public static IApplicationBuilder UseLimiting(this IApplicationBuilder app)
        {
            //将中间件加入管道模型
            return app.UseMiddleware<LimitingMiddleWare>();
        }
    }
}
