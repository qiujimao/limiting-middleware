﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace Limiting.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddRedisCatchLimiting(this IServiceCollection services,
            Action<LimitingOptions> optionsAction = null)
        {
            var options = new LimitingOptions();
            optionsAction?.Invoke(options);

            services.AddSingleton(_ => options);
            services.AddSingleton<ILimiting, RedisCatchLimiting>();

            return services;
        }
        public static IServiceCollection AddMemoryCatchLimiting(this IServiceCollection services,
            Action<LimitingOptions> optionsAction = null)
        {
            var options = new LimitingOptions();
            optionsAction?.Invoke(options);

            services.AddSingleton(_ => options);
            services.AddSingleton<ILimiting, MemoryCatchLimiting>();

            return services;
        }
    }
}
