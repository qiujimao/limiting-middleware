﻿using System.Threading.Tasks;

namespace Limiting
{
    public interface ILimiting
    {
        Task<bool> CheckIdentityLimited(string identity);
    }
}