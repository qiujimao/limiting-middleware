# 一个限流中间件

#### 介绍

代码量少、使用简单、注释齐全、超高扩展。

#### 支持的缓存架构

- Redis ⛳

- MemoryCatch ⛳

#### Demo 示例

- 最简单的使用

```C#
public void ConfigureServices(IServiceCollection services)
{
    services.AddMemoryCache()
            .AddMemoryCatchLimiting();
}

public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
{
      app.UseLimiting();
}

```

- DIY 一下

```C#
public void ConfigureServices(IServiceCollection services)
{
    //services.AddMemoryCache()
    //   .AddMemoryCatchLimiting();
    var redis = ConnectionMultiplexer.Connect(Configuration.GetConnectionString("Redis"));
    services.AddSingleton(redis.GetDatabase())
        .AddRedisCatchLimiting(o =>
        {
            //限制次数                                             
            o.Limit = 5;
            //约定时间
            o.Time = TimeSpan.FromSeconds(10);
            //设置唯一标识的委托
            o.IdentityDelegate = context =>
            {
                //自定义返回唯一键
                //return context.User.Claims...
                //return context.Request.Cookies...
                return context.Request.HttpContext
                     .Connection.RemoteIpAddress.MapToIPv4().ToString();
            };
            //回调委托
            o.CallbackDelegate = async context =>
            {
                context.Response.StatusCode = 503;
                string s = "哈哈哈，被限流了！";
                await context.Response.WriteAsync(s);
            };
            //白名单
            o.Allows = new []
            {
                "0.0.0.1"
            };
        });
}
public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
{
    app.UseLimiting();
}
```
