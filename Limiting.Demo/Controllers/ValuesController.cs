﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace Limiting.Demo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        [HttpGet]
        public ActionResult Get()
        {
            return Ok(new { message = "PHP是世界上最好的语言！", time = DateTime.Now });
        }
    }
}
