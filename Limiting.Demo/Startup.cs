using Limiting.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis;
using System;
using System.Linq;

namespace Limiting.Demo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddMemoryCache()
            //   .AddMemoryCatchLimiting();
            var redis = ConnectionMultiplexer.Connect(Configuration.GetConnectionString("Redis"));
            services.AddSingleton(redis.GetDatabase())
                .AddRedisCatchLimiting(o =>
                {
                    //限制次数                                             
                    o.Limit = 5;
                    //约定时间
                    o.Time = TimeSpan.FromSeconds(10);
                    //设置唯一标识的委托
                    o.IdentityDelegate = context =>
                    {
                        //自定义返回唯一键
                        //return context.User.Claims...
                        //return context.Request.Cookies...

                        return context.Request.HttpContext
                             .Connection.RemoteIpAddress.MapToIPv4().ToString();
                    };
                    //回调委托
                    o.CallbackDelegate = async context =>
                    {
                        context.Response.StatusCode = 503;
                        string s = "哈哈哈，被限流了！";
                        await context.Response.WriteAsync(s);
                    };
                    //白名单
                    o.Allows = new[]
                    {
                        "0.0.0.1"
                    };
                });

            services.AddControllers();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseLimiting();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
